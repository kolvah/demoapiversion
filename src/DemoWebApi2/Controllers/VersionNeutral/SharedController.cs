﻿using Microsoft.Web.Http;
using System.Web.Http;

namespace DemoWebApi.Controllers.VersionNeutral
{
    [ApiVersionNeutral]
    [RoutePrefix("shared")]
    public class SharedController : ApiController
    {
        [HttpGet]
        [Route("hello")]
        public string Get()
        {
            return "hello shared";
        }
    }
}
