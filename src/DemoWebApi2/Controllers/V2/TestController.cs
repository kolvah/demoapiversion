﻿using Microsoft.Web.Http;
using System.Web.Http;

namespace DemoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    [RoutePrefix("test")]
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("hello")]
        public string Get()
        {
            return "hello test v2";
        }
    }
}