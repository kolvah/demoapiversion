﻿using Microsoft.Web.Http;
using Microsoft.Web.Http.Routing;
using Swashbuckle.Application;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace DemoWebApi2
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);   // версия по умолчанию
                options.AssumeDefaultVersionWhenUnspecified = true; // использовать версию по умолчанию если не установлена
                options.ReportApiVersions = true;                   // отправка номера версии в заголовках ответа
            });

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.MapHttpAttributeRoutes(new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                {
                    ["apiVersion"] = typeof(ApiVersionRouteConstraint)
                }
            });

            var apiExplorer = config.AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });


            config.EnableSwagger("{apiVersion}/swagger",
                swagger =>
                {
                    swagger.MultipleApiVersions(
                        (apiDescription, version) => apiDescription.GetGroupName() == version,
                        info =>
                        {
                            foreach (var group in apiExplorer.ApiDescriptions)
                            {
                                info.Version(group.Name, $"Sample API {group.ApiVersion}");
                            }
                        });
                })
                .EnableSwaggerUi(swagger => swagger.EnableDiscoveryUrlSelector());
        }
    }
}
