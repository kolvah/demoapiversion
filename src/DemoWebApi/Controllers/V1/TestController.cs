﻿using Microsoft.Web.Http;
using System.Web.Http;

namespace DemoWebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [RoutePrefix("v{version:apiVersion}/test")]
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("hello")]        
        public string Get()
        {
            return "hello test v1";
        }
    }
}
