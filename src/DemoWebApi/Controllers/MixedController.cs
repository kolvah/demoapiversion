﻿using Microsoft.Web.Http;
using System.Web.Http;

namespace DemoWebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [RoutePrefix("v{version:apiVersion}/mixed")]
    public class MixedController : ApiController
    {
        [HttpGet]
        [Route("hello")]
        [MapToApiVersion("1.0")]
        public string GetV1()
        {
            return "hello mixed v1";
        }
              
        [HttpGet]
        [Route("hello")]
        [MapToApiVersion("2.0")]
        public string GetV2()
        {
            return "hello mixed v2";
        }

        [HttpGet]
        [Route("common")]
        public string Get()
        {
            return "hello mixed common";
        }

        [HttpGet]
        [Route("test")]
        [MapToApiVersion("1.0")]
        public string Test()
        {
            return "test result";
        }


    }
}
